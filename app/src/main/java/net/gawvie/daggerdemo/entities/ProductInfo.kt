package net.gawvie.daggerdemo.entities

data class ProductInfo(var id: Int? = -1,
                       var name: String? = null,
                       var amount: Int? = -1,
                       var description: String? = null)