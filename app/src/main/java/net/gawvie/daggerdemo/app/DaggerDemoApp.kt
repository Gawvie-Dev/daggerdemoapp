package net.gawvie.daggerdemo.app

import android.app.Activity
import android.app.Application
import android.content.SharedPreferences
import android.support.v4.app.Fragment
import android.util.Log
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import net.gawvie.daggerdemo.di.app.DaggerAppComponent
import javax.inject.Inject

class DaggerDemoApp : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var sharedPref: SharedPreferences

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder().create(this).build().inject(this)
        sharedPref.edit().putBoolean("PREF_IS_MAIN", true)
                .apply()

        Log.d("DaggerDemoApp", "onCreate # PREF_IS_MAIN: " +
                "${sharedPref.getBoolean("PREF_IS_MAIN", false)}")
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector
}