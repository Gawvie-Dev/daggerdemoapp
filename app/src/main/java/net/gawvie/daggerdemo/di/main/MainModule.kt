package net.gawvie.daggerdemo.di.main

import dagger.Module
import dagger.Provides
import net.gawvie.daggerdemo.entities.ProductInfo
import javax.inject.Named

@Module
class MainModule {

    @Provides
    @Named("mainProductInfo")
    fun provideMainProductInfo(): ProductInfo = ProductInfo(
            1, "Main ProductName", 100, "mainProductDescription")

    @Provides
    @Named("homeProductInfo")
    fun provideHomeProductInfo(): ProductInfo = ProductInfo(
            2, "Home ProductName", 240, "homeProductDescription")
}