package net.gawvie.daggerdemo.di.app

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.gawvie.daggerdemo.activities.MainActivity
import net.gawvie.daggerdemo.di.main.MainModule

@Module
abstract class ActivitiesBindingModule {

    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun mainActivity(): MainActivity
}