package net.gawvie.daggerdemo.di.app

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import net.gawvie.daggerdemo.app.DaggerDemoApp
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class,
    ActivitiesBindingModule::class, FragmentBindingModule::class])
interface AppComponent : AndroidInjector<DaggerDemoApp> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun create(app: Application): Builder

        fun build(): AppComponent
    }
}