package net.gawvie.daggerdemo.di.app

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.gawvie.daggerdemo.di.main.MainModule
import net.gawvie.daggerdemo.fragment.HomeDialogFragment

@Module
abstract class FragmentBindingModule {
    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun homeFragment(): HomeDialogFragment
}