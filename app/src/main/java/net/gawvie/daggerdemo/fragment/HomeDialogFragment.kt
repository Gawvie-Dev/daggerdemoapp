package net.gawvie.daggerdemo.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_dialog.*
import net.gawvie.daggerdemo.R
import net.gawvie.daggerdemo.entities.ProductInfo
import javax.inject.Inject
import javax.inject.Named

class HomeDialogFragment : AppCompatDialogFragment() {

    var onButtonClicked: ((dialog: HomeDialogFragment) -> Unit)? = null

    @Inject
    lateinit var sharedPref: SharedPreferences

    @Inject
    @field:Named("homeProductInfo")
    lateinit var productInfo: ProductInfo

    companion object {
        fun newInstance(): HomeDialogFragment = HomeDialogFragment()
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_dialog, container, false)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        isCancelable = false
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onStart() {
        super.onStart()
        dialog.window?.apply {
            setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    private fun initView() {
        homeDialogButton.setOnClickListener { onButtonClicked?.invoke(this) }
        homeDialogTitleTextView.text = String.format("PREF_IS_MAIN: ${sharedPref.getBoolean("PREF_IS_MAIN", false)}")
        homeDialogMessageTextView.text = "${productInfo.name}\n${productInfo.description}"
    }
}