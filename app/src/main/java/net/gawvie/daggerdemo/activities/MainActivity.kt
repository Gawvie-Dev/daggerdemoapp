package net.gawvie.daggerdemo.activities

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import net.gawvie.daggerdemo.R
import net.gawvie.daggerdemo.entities.ProductInfo
import net.gawvie.daggerdemo.fragment.HomeDialogFragment
import javax.inject.Inject
import javax.inject.Named

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var sharedPref: SharedPreferences

    @Inject
    lateinit var dispatchingFragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    @field:Named("mainProductInfo")
    lateinit var productInfo: ProductInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AndroidInjection.inject(this)

        Toast.makeText(this, "PREF_IS_MAIN: ${sharedPref.getBoolean("PREF_IS_MAIN", false)}",
                Toast.LENGTH_SHORT).show()

        initView()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingFragmentInjector

    private fun initView() {
        mainTextView.text = "Product: ${productInfo.name}"

        mainMoreInfoButton.setOnClickListener {
            showMoreInfoDialog()
        }
    }

    private fun showMoreInfoDialog() {
        HomeDialogFragment.newInstance()
                .apply {
                    onButtonClicked = { dismiss() }
                    show(supportFragmentManager, "HomeDialogFragment")
                }
    }
}
